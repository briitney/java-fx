package javafxapp;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MainWindow extends Application {

	final long startNanoTime = System.nanoTime();
	ArrayList<String> input = new ArrayList<String>();
	int arrowAdj = 0;
	int arrowTote = 0;
	long rightStart = 0;
	long leftStart = 0;

	@Override
	public void start(Stage stage) {

		BorderPane root = new BorderPane();
		Scene scene = new Scene(root);
		stage.setScene(scene);

		Button btn = new Button("X");

		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				stage.close();
			}
		});

		HBox filler = new HBox();
		HBox.setHgrow(filler, Priority.ALWAYS);

		ToolBar toolbar = new ToolBar(filler, btn);

		root.setTop(toolbar);

		Canvas canvas = new Canvas();
		canvas.widthProperty().bind(scene.widthProperty());
		canvas.heightProperty().bind(scene.heightProperty().subtract(toolbar.heightProperty()));
		root.setCenter(canvas);
		canvas.setFocusTraversable(true);
		canvas.requestFocus();

		GraphicsContext gc = canvas.getGraphicsContext2D();

		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent e) {
				String code = e.getCode().toString();

				// only add once... prevent duplicates
				if (!input.contains(code)) {
					input.add(code);
					if (code == "RIGHT") {
						rightStart = System.nanoTime();
					} else if (code == "LEFT") {
						leftStart = System.nanoTime();
					}
				}
				e.consume();
			}
		});

		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent e) {
				String code = e.getCode().toString();
				input.remove(code);
				if (code == "RIGHT" || code == "LEFT") {
					arrowTote = arrowAdj;
				}
			}
		});

		InvalidationListener resizeListener = new InvalidationListener() {
			public void invalidated(Observable observable) {
				drawHello(gc, (int) canvas.getWidth(), (int) canvas.getHeight());
			}
		};

		scene.widthProperty().addListener(resizeListener);
		scene.heightProperty().addListener(resizeListener);
		
		drawHello(gc, (int) canvas.getWidth(), (int) canvas.getHeight());

		stage.show();
	}

	public void drawHello(GraphicsContext gc, int w, int h) {
		double smallerSide = (w < h) ? w : h;
		new AnimationTimer() {
			public void handle(long currentNanoTime) {
				
				gc.clearRect(0, 0, w, h);

				double t = (currentNanoTime - startNanoTime) / 1000000000.0;
			
                if (input.contains("RIGHT")) {
                	arrowAdj = (int)((currentNanoTime - rightStart) / 5000000.0) + arrowTote;
                } else if (input.contains("LEFT")) {
                	arrowAdj = -(int)((currentNanoTime - leftStart) / 5000000.0) + arrowTote;
                }
						
				double x = w / 2 - 50 + (smallerSide / 4 + arrowAdj) * Math.cos(4 * t);
				double y = h / 2 - 50 + (smallerSide / 4 + arrowAdj) * Math.sin(4 * t);

				if (input.contains("C"))
					gc.setFill(Color.RED);
				else
					gc.setFill(Color.BLACK);

				gc.fillOval(x, y, 100, 100);
			}
		}.start();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
